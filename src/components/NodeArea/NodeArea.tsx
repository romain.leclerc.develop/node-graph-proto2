import React from 'react';
import { useDrop } from 'react-dnd';
import { useSelector } from 'react-redux';
import { RootState } from '../../redux/store'; 
import Node from '../Node';
import Connection from '../Connection';
import AddNodesButton from '../Lib/AddNodesButton/AddNodesButton';

import './NodeArea.css'

interface DropItem {
  id: string;
  type: string;
}



const NodeArea: React.FC = () => {

  const nodes = useSelector((state: RootState) => state.nodes);
  const connections = useSelector((state: RootState) => state.connections);


  const [, drop] = useDrop({
    accept: 'node',
    drop: (item: DropItem, monitor) => ({
      x: monitor.getClientOffset()?.x,
      y: monitor.getClientOffset()?.y,
    }),
  });

  return (
    <div ref={drop} className='node-area'>
      <AddNodesButton />
      {Object.values(nodes).map((node) => (
        <Node key={node.id} id={node.id} node={node} />
      ))}
      {Object.values(connections).map((connection) => (
        <Connection key={connection.id} connection={connection} />
      ))}
    </div>
  );
};

export default NodeArea;