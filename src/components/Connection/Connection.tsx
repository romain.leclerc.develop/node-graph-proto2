import React from 'react';
import { Connection as ConnectionType } from '../../types'; // Import your types
import { useSelector } from 'react-redux';
import { RootState } from '../../redux/store';

import './Connection.css'
interface ConnectionProps {
  connection: ConnectionType;
}

const Connection: React.FC<ConnectionProps> = ({ connection }) => {
  const nodes = useSelector((state: RootState) => state.nodes);

  const sourceNode = nodes[connection.source.nodeId];
  const targetNode = nodes[connection.target.nodeId];

  const sourcePort = sourceNode.outputs.find(output => output.portId === connection.source.outputPortId);
  const targetPort = targetNode.inputs.find(input => input.portId === connection.target.inputPortId);

  if (!sourcePort || !targetPort) {
    return null; // Or some other default value or error handling
  }
  
  return (
    <svg className="connection">
      <line
        x1={sourceNode.x + sourcePort.x}
        y1={sourceNode.y + sourcePort.y}
        x2={targetNode.x + targetPort.x}
        y2={targetNode.y + targetPort.y}
        stroke="white"
      />
    </svg>
  );
};

export default Connection;