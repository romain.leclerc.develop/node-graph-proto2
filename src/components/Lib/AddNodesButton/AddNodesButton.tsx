import React from 'react';
import { useDispatch } from 'react-redux';
import { addNode } from '../../../redux/actions';
import './AddNodesButton.css'

const node1 = {
  id: "node1",
  nodetype: "type1",
  x: 100,
  y: 200,
  inputs: [
    { 
      portId: "port1", 
      type: "input",
      properties: {},
      x: 0, // Add this
      y: 0, // Add this
    }
  ],
  outputs: [
    { 
      portId: "port2", 
      type: "output",
      properties: {},
      x: 0, // Add this
      y: 0, // Add this
    }
  ]
};

const node2 = {
  id: "node2",
  nodetype: "type2",
  x: 300,
  y: 400,
  inputs: [
    { 
      portId: "port3", 
      type: "input",
      properties: {},
      x: 0, // Add this
      y: 0, // Add this
    }
  ],
  outputs: [
    { 
      portId: "port4", 
      type: "output",
      properties: {},
      x: 0, // Add this
      y: 0, // Add this
    }
  ]
};

const AddNodesButton: React.FC = () => {
    const dispatch = useDispatch();
  
    const handleClick = () => {
      dispatch(addNode(node1));
      dispatch(addNode(node2));
    };
  
    return (
      <button className="button" onClick={handleClick}>Add Nodes</button>
    );
  };

export default AddNodesButton;