import React from 'react';
import { Port as PortType } from '../../types'; // Import your types

import './Port.css'


const Port: React.FC<PortType> = ( port ) => {

    const { portId, type, properties } = port;
    const { disable } = properties;
  return (
    <div>
        {type === 'input' && 
            <div className='container-port'>
                <div className={disable ? "port disable-port" : "port input-port"} />
                <p className='label'>{portId}</p>
            </div>
        }
        {type === 'output' && 
            <div className='container-port'>
                <p className='label'>{portId}</p>
                <div className={disable ? "port disable-port" : "port output-port"} />
            </div>
        }
    </div>
  );
};

export default Port;