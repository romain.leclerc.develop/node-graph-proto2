import React, { useRef } from 'react';
import { useDrag, DragSourceMonitor } from 'react-dnd';
import { useDispatch } from 'react-redux';
import { Node as NodeType } from '../../types'; // Import your types
import { moveNode } from '../../redux/actions'; // Assuming you have an action creator like this
import Port from '../Port';
import { AppDispatch } from '../../redux/store';

import './Node.css'


interface NodeProps {
  id: string;
  node: NodeType;
}

interface DropResult {
  x: number;
  y: number;
  // Ajoutez ici toutes les autres propriétés que vous attendez du résultat du dépôt
}

interface DragItem {
  id: string;
}

const Node: React.FC<NodeProps> = ({ id, node }) => {
  const dispatch = useDispatch<AppDispatch>();
  const ref = useRef<HTMLDivElement>(null);
  const offset = useRef<{ offsetX: number, offsetY: number }>({ offsetX: 0, offsetY: 0 });

  const [{ isDragging }, drag] = useDrag<DragItem, {}, { isDragging: boolean }>({
    type: 'node',
    item: { id },
    end: (item: DragItem | undefined, monitor: DragSourceMonitor) => {
      const dropResult = monitor.getDropResult<DropResult>();
      if (item && dropResult) {
        dispatch(moveNode(node.id, dropResult.x - offset.current.offsetX, dropResult.y - offset.current.offsetY));
      }
    },
    collect: (monitor) => ({
      isDragging: !!monitor.isDragging(),
    }),
  });

  drag(ref);

  return (
    <div 
      ref={ref} 
      onMouseDown={event => {
        const nodeRect = ref.current?.getBoundingClientRect();
        if (nodeRect) {
          offset.current = {
            offsetX: event.clientX - nodeRect.left,
            offsetY: event.clientY - nodeRect.top
          };
        }
      }}
      className="node" 
      style={{ opacity: isDragging ? 0.5 : 1, left: `${node.x}px`, top: `${node.y}px` }}
    >
      <div className="node-info">
        <div>ID: {node.id}</div>
        <div>X: {node.x}</div>
        <div>Y: {node.y}</div>
      </div>
      <div className="node-ports">
        <div >
          {node && node.inputs && node.inputs.map((port) => (
            <Port 
              key={port.portId} 
              portId={port.portId} 
              properties={port.properties} 
              type={port.type} 
              x={port.x}  
              y={port.y}  
            />
          ))}
        </div>
        <div >
          {node && node.outputs && node.outputs.map((port) => (
            <Port 
              key={port.portId}
              portId={port.portId} 
              properties={port.properties} 
              type={port.type} 
              x={port.x}  
              y={port.y}  
            />
          ))}
        </div>
      </div>
    </div>
  );
};

export default Node;