import { Action } from 'redux';
import {UPDATE_UI} from '../redux/actions';

export interface UI {
  id: string;
  // Ajoutez ici les autres propriétés de votre UI
}

export interface UIState {
  [id: string]: UI;
}


export interface UpdateUIAction extends Action<typeof UPDATE_UI> {
  payload: UI;
}