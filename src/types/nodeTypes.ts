import { Action } from 'redux';
import {ADD_NODE, UPDATE_NODE, DELETE_NODE, MOVE_NODE} from '../redux/actions';

export interface Port {
  portId: string;
  type: string;
  properties: any;
  x: number; // Add this
  y: number; // Add this
}

export interface Node {
  id: string;
  nodetype: string;
  x: number;
  y: number;
  inputs: Port[];
  outputs: Port[];
}

export interface NodeState {
  [id: string]: Node;
}

interface AddNodeAction extends Action<typeof ADD_NODE> {
  payload: Node;
}

interface UpdateNodeAction extends Action<typeof UPDATE_NODE> {
  payload: Node;
}

export interface DeleteNodeAction extends Action<typeof DELETE_NODE> {
  payload: string;
}

interface MoveNodeAction extends Action<typeof MOVE_NODE> {
  payload: {
    nodeId: string;
    x: number;
    y: number;
  };
}


export type NodeAction = AddNodeAction | UpdateNodeAction | DeleteNodeAction | MoveNodeAction ;