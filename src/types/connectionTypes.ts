import { Action } from 'redux';
import {ADD_CONNECTION,  REMOVE_CONNECTION} from '../redux/actions';

export interface Connection {
  id: string;
  source: {
    nodeId: string;
    outputPortId: string;
  };
  target: {
    nodeId: string;
    inputPortId: string;
  };
}

export interface Connections {
  [id: string]: Connection;
}

export interface AddConnectionAction extends Action<typeof ADD_CONNECTION> {
  payload: Connection;
}

export interface RemoveConnectionAction extends Action<typeof REMOVE_CONNECTION> {
  payload: {
    id: string;
  };
}

export type ConnectionAction = AddConnectionAction | RemoveConnectionAction;
