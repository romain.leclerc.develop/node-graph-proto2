import React from 'react';

import { Provider } from 'react-redux';
import store from './redux/store';

import { DndProvider } from 'react-dnd';
import { HTML5Backend } from 'react-dnd-html5-backend';

import NodeArea from './components/NodeArea';

const App: React.FC = () => {
  return (
    <Provider store={store}>
      <div className="App">
      <DndProvider backend={HTML5Backend}>
        <NodeArea />
      </DndProvider>
      </div>
    </Provider>
  );
}

export default App;
