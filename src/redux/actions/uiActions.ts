import { UI } from '../../types';

export const UPDATE_UI = 'UPDATE_UI';

export const updateUI = (ui: UI) => ({
  type: UPDATE_UI,
  payload: ui,
});
