import { Node } from '../../types';
import { Dispatch } from 'redux';
import { RootState } from '../store';

export const ADD_NODE = 'ADD_NODE';
export const UPDATE_NODE = 'UPDATE_NODE';
export const DELETE_NODE = 'DELETE_NODE';
export const MOVE_NODE = 'MOVE_NODE';


export const addNode = (node: Node) => ({
  type: ADD_NODE,
  payload: node,
});

export const updateNode = (node: Node) => ({
  type: UPDATE_NODE,
  payload: node,
});

export const deleteNode = (id: String) => ({
  type: DELETE_NODE,
  payload: id,
});

export const moveNode = (nodeId: string, x: number, y: number) => (dispatch: Dispatch, getState: () => RootState) => {
  
  dispatch({
    type: MOVE_NODE,
    payload: { nodeId, x, y },
  });

};

