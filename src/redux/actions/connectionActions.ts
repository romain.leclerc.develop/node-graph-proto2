import { AddConnectionAction, RemoveConnectionAction } from '../../types';

export const ADD_CONNECTION = 'ADD_CONNECTION';
export const REMOVE_CONNECTION = 'REMOVE_CONNECTION';

export const addConnection = (sourceNodeId: string, sourceOutputId: string, targetNodeId: string, targetInputId: string): AddConnectionAction => {
  return {
    type: ADD_CONNECTION,
    payload: {
      id: 'conn_' + Date.now(),  // or any other unique id generator
      source: { nodeId: sourceNodeId, outputPortId: sourceOutputId },
      target: { nodeId: targetNodeId, inputPortId: targetInputId },
    },
  };
};

export const removeConnection = (connectionId: string): RemoveConnectionAction => {
  return {
    type: REMOVE_CONNECTION,
    payload: {
      id: connectionId,
    },
  };
};