import { combineReducers } from 'redux';
import nodesReducer from './reducers/nodesReducer';
import uiReducer from './reducers/uiReducer';
import connectionsReducer from './reducers/connectionsReducer';

const rootReducer = combineReducers({
  nodes: nodesReducer,
  UI: uiReducer,
  connections: connectionsReducer,
});

export default rootReducer;