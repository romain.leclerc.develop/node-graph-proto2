import { Connections, ConnectionAction } from '../../types';
import { ADD_CONNECTION, REMOVE_CONNECTION } from '../actions';

const initialState: Connections = {};

const connectionReducer = (state = initialState, action: ConnectionAction): Connections => {
  switch (action.type) {
    case ADD_CONNECTION:
      return {
        ...state,
        [action.payload.id]: action.payload,
      };
    case REMOVE_CONNECTION:
      const { [action.payload.id]: _, ...newState } = state;
      return newState;
    default:
      return state;
  }
};

export default connectionReducer;


