import { UIState, UpdateUIAction } from '../../types';
import { UPDATE_UI } from '../actions';

const initialState: UIState = {};

const uiReducer = (state: UIState = initialState, action: UpdateUIAction): UIState => {
  switch (action.type) {
    case UPDATE_UI:
      return { ...state, [action.payload.id]: { ...state[action.payload.id], ...action.payload } };
    default:
      return state;
  }
};

export default uiReducer;