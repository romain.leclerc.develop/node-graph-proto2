import { NodeState, NodeAction} from '../../types';
import { ADD_NODE, UPDATE_NODE, DELETE_NODE, MOVE_NODE} from '../actions';


const initialState: NodeState = {};

const nodeReducer = (state: NodeState = initialState, action: NodeAction): NodeState => {
  switch (action.type) {
    case ADD_NODE:
      return {
        ...state,
        [action.payload.id]: action.payload,
      };
    case UPDATE_NODE:
      return {
        ...state,
        [action.payload.id]: action.payload,
      };
    case DELETE_NODE:
      const { [action.payload]: _, ...newState } = state;
      return newState;
    case MOVE_NODE:
      return {
        ...state,
        [action.payload.nodeId]: {
          ...state[action.payload.nodeId],
          x: action.payload.x,
          y: action.payload.y,
        },
      };
    default:
      return state;
  }
}

export default nodeReducer;