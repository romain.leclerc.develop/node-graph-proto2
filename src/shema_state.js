{
  UI: {
    item1: {
      nodeArea: {
        nodes: ['node_UniqueId1', 'node_UniqueId2'],
        selectedNode: 'node_UniqueId1',
        width: 300,
        height: 400,
        docked: true,
        visible: true,
      },
      bridge: {
        outputsNodes: {},
        outputsWidget: {},
      },
      widget:{
        width: 300,
        height: 400,
        docked: true,
        visible: true,
      }
    },
    item2: {
      nodeArea: {
        nodes: ['node_UniqueId3', 'node_UniqueId4'],
        selectedNode: 'node_UniqueId3',
        width: 300,
        height: 400,
        docked: true,
        visible: true,
      },
      bridge: {
        outputsNodes: {},
        outputsWidget: {},
      },
      widget:{
        width: 300,
        height: 400,
        docked: true,
        visible: true,
      }
    },
  },
  nodes: {
    'node_UniqueId1': {
      id: 'node_UniqueId1',
      type: 'nodeType1',
      x: 100,
      y: 100,
      collapsed: false,
      selected: false,
      action: {},
      inputs: ['input_UniqueId1', 'input_UniqueId2'],
      outputs: ['output_UniqueId1', 'output_UniqueId2'],
    },
    'node_UniqueId2': {
      id: 'node_UniqueId2',
      type: 'nodeType1',
      x: 200,
      y: 100,
      collapsed: false,
      selected: false,
      action: {},
      inputs: ['input_UniqueId3', 'input_UniqueId4'],
      outputs: ['output_UniqueId3', 'output_UniqueId4'],
    },
    // etc.
  },
  connections: {
    'conn_UniqueId1': {
      id: 'conn_UniqueId1',
      source: {
        nodeId: 'node_UniqueId1',
        outputId: 'output_UniqueId1'
      },
      target: {
        nodeId: 'node_UniqueId2',
        inputId: 'input_UniqueId3'
      }
    },
    'conn_UniqueId2': {
      id: 'conn_UniqueId2',
      source: {
        nodeId: 'node_UniqueId1',
        outputId: 'output_UniqueId2'
      },
      target: {
        nodeId: 'node_UniqueId2',
        inputId: 'input_UniqueId4'
      }
    },
    // etc.
  },
  // Autres états nécessaires...
}