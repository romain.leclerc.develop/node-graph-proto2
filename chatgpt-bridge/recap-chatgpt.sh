#!/bin/bash

# Lire les variables du fichier de configuration
source config.cfg

# Créer un nom de fichier unique pour le fichier de récapitulatif
current_date=$(date +"%Y%m%d_%H%M%S")
recap_file="recap_$current_date.txt"

# Afficher le contenu du fichier de configuration

echo "Configuration utilisée :" > $output_dir/$recap_file
cat config.cfg >> $output_dir/$recap_file
echo " " >> $output_dir/$recap_file
echo "//////////////////////////////////////////////////////////////////////////" >> $output_dir/$recap_file
echo " " >> $output_dir/$recap_file

# Informations de base du projet
echo "Nom du Projet: $project_name" >> $output_dir/$recap_file
echo "Date / Heure: $(date)" >> $output_dir/$recap_file

# Vérifie si l'argument -t a été donné
generate_tree=false
while getopts "t" opt; do
  case ${opt} in
    t)
      generate_tree=true
      ;;
    \?)
      echo "Usage: cmd [-t]"
      ;;
  esac
done

# Arborescence
if $generate_tree ; then
  echo "Arborescence :" >> $output_dir/$recap_file
  print_dirs() {
      local indents="$2"
      for item in "$1"/*; do
          if [ -d "$item" ]; then
              echo "${indents}${item##*/}" >> $output_dir/$recap_file
              print_dirs "$item" "${indents}-"
          else
              echo "${indents}${item##*/}" >> $output_dir/$recap_file
          fi
      done
  }
  print_dirs $src_dir ""
fi

# Contenu des fichiers
echo "Contenus des fichiers: ${files_to_check[*]}" >> $output_dir/$recap_file
print_file_content() {
    for item in "$1"/*; do
        if [ -d "$item" ]; then
            print_file_content "$item"
        else
            for file in "${files_to_check[@]}"
            do
                if [ "${item##*/}" = "$file" ]; then
                    echo "////// $file /////////" >> $output_dir/$recap_file
                    cat "$item" >> $output_dir/$recap_file
                fi
            done
        fi
    done
}
print_file_content $src_dir